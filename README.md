# README #
### Project description ###

Demonstrative project which displays user and a form to update a user.
Uses JSON placeholder fake api to fetch and manipulate the data.


### How do I get set up? ###

```bash
# 1. Ensure you have the correct node/npm versions node v14.16.1 (npm v6.14.12)
$ nvm use --lts
# 2. Install node_modules
$ npm i

# 3. Environment setup
$ Create a .env.development file in the root directory
$ Paste REACT_APP_API_URL=https://jsonplaceholder.typicode.com

# 4. Starting the application
$ npm start

# optional commands
$ npm run lint
$ npm run typecheck
$ npm run stylelint
$ npm run test
```


### Who do I talk to? ###

* Email: alexander.leismann@tptlive.ee
