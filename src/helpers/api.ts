import Api, { ApiInterface } from 'helpers/constructors/api';

const baseUrl = process.env.REACT_APP_API_URL || '';

const handleError = (): void => {
  alert('something went wrong'); // could be handled with notification
};

type APIFactory = { getInstance: () => ApiInterface };

// Optional Singleton Optimisation for api  https://www.dofactory.com/javascript/design-patterns/singleton
const ApiFactory = ((): APIFactory => {
  let apiInstance: ApiInterface | null = null;

  return {
    getInstance: (): ApiInterface => {
      // checks if instance already created
      if (apiInstance === null) {
        // if not create an instance
        apiInstance = new Api(
          baseUrl,
          handleError,
        );
      }

      // return the created instance on all subsequent calls
      return apiInstance;
    },
  };
})();

export default ApiFactory.getInstance();
