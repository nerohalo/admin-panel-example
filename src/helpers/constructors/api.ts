/* eslint-disable */
import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';

export interface SuccessfulResponse<T> {
  body: T;
}

export interface FailedResponse {
  error?: boolean;
}

export interface ApiInterface {
  acceptLanguage: string | null;
  authorizationToken: string | null;

  post: <RES, REQ = unknown>(url: string, data: REQ)
    => Promise<RES>;
  get: <RES, REQ = unknown>(url: string, params: REQ)
    => Promise<RES>;
  put: <RES, REQ = unknown>(url: string, data: REQ)
    => Promise<RES>;
  patch: <RES, REQ = unknown>(url: string, data: REQ)
    => Promise<RES>;
  delete: <RES, REQ = unknown>(url: string, data: REQ)
    => Promise<RES>;
}

class Api implements ApiInterface {
  private readonly baseURL: string;

  private readonly errorHandler: () => void;

  private _acceptLanguage: string | null = null;

  set acceptLanguage(value: string | null) {
    this._acceptLanguage = value;
  }

  get acceptLanguage(): string | null {
    return this._acceptLanguage;
  }

  private _authorizationToken: string | null = null;

  get authorizationToken(): string | null {
    return this._authorizationToken;
  }

  set authorizationToken(value: string | null) {
    this._authorizationToken = value;
  }


  constructor(
    baseURL: string,
    errorHandler: () => void,
  ) {
    this.baseURL = baseURL;
    this.errorHandler = errorHandler;
  }

  private readonly handleCatch = (): void => {
    this.errorHandler();
  };

  private readonly makeHeaders = (): AxiosRequestConfig['headers'] => ({
    // Don't send headers unless the value is present

    ...(this._acceptLanguage ? {
      'Accept-Language': this._acceptLanguage,
    } : {}),

    ...(this._authorizationToken ? {
      Authorization: `Bearer ${this._authorizationToken}`,
    } : {}),
  });

  private readonly request = <RES, REQ>(
    axiosConfig: AxiosRequestConfig,
  ): Promise<RES> => axios({
    baseURL: this.baseURL,
    headers: this.makeHeaders(),
    withCredentials: false,
    validateStatus: (status) => status === 200, // 200 is only successful response
    ...axiosConfig,
  })
    .then((axiosResponse: AxiosResponse<RES>) => axiosResponse.data)
    .catch(() => Promise.reject(this.handleCatch()));

  public post = <RES, REQ = unknown>(
    url: string,
    data: REQ,
  ): Promise<RES> => this.request<RES, REQ>({
    url,
    data,
    method: 'post',
  });

  public put = <RES, REQ = unknown>(
    url: string,
    data: REQ,
  ): Promise<RES> => this.request<RES, REQ>({
    url,
    data,
    method: 'put',
  });

  public get = <RES, REQ = unknown>(
    url: string,
    params: REQ,
  ): Promise<RES> => this.request<RES, REQ>({
    url,
    params,
    method: 'get',
  });

  public delete = <RES, REQ = unknown>(
    url: string,
    data: REQ,
  ): Promise<RES> => this.request<RES, REQ>({
    url,
    data,
    method: 'delete',
  });

  public patch = <RES, REQ = unknown>(
    url: string,
    data: REQ,
  ): Promise<RES> => this.request<RES, REQ>({
    url,
    data,
    method: 'patch',
  });
}

export default Api;
