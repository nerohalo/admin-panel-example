import React from 'react';

import C from 'helpers/constants';

type sortKeyType<T> = keyof T | null
type sortOrderType = string | null

// eslint-disable-next-line import/prefer-default-export
export const useSortableData = <T>(items: T[], sortKey: sortKeyType<T>, sortOrder: sortOrderType): T[] => React.useMemo(() => {
  const sortableItems = [...items];
  if (sortKey === null) {
    return sortableItems;
  }

  return sortableItems.sort((a, b) => {
    const isAscending = sortOrder === C.SORT_ORDER.ASC;

    if (a[sortKey] < b[sortKey]) {
      return isAscending ? -1 : 1;
    }

    if (a[sortKey] > b[sortKey]) {
      return isAscending ? 1 : -1;
    }

    return 0;
  });
}, [items, sortOrder, sortKey]);
