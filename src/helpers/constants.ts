export default {
  SORT_ORDER: {
    ASC: 'ASC',
    DESC: 'DESC',
  },
  ACTION_TYPES: {
    USER_SET: 'USER_SET',
    USER_UPDATE: 'USER_UPDATE',
    USER_DELETE: 'USER_DELETE',
  },
} as const;
