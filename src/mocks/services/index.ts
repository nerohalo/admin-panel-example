import { Mock, ServiceProvider } from 'types';

import { mockPlaceholderService } from './placeholderService';

export const mockServiceProvider: Mock<ServiceProvider> = (customMocks = {}) => ({
  placeholderService: mockPlaceholderService(),
  ...customMocks,
});

export * from './placeholderService';
