import { Mock, PlaceholderService, User } from 'types';

import { userFixture } from 'fixtures';

// eslint-disable-next-line import/prefer-default-export
export const mockPlaceholderService: Mock<PlaceholderService> = (customMocks) => ({
  fetchUsers: async (): Promise<User[]> => [userFixture()],
  updateUser: async (): Promise<void> => {},
  deleteUser: async (): Promise<void> => {},
  ...customMocks,
});
