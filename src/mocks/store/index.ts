import configureMockStore, { MockStoreEnhanced } from 'redux-mock-store';
import thunk from 'redux-thunk';

import {
  ApplicationState, ServiceProvider, DispatchExts, ThunkMiddlewareType,
} from 'types';

// eslint-disable-next-line import/prefer-default-export
export const mockStore = (initialState: ApplicationState, serviceProvider: ServiceProvider): MockStoreEnhanced<ApplicationState, DispatchExts> => {
  const middlewares = [thunk.withExtraArgument(serviceProvider) as ThunkMiddlewareType];
  const mockReduxStore = configureMockStore<ApplicationState, DispatchExts>(middlewares);

  return mockReduxStore(initialState);
};
