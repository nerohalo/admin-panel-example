import { ServiceProvider } from 'types';

import placeholderService from 'services/PlaceholderService';

const serviceProvider: ServiceProvider = {
  placeholderService,
};

export default serviceProvider;
