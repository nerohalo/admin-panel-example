import {
  PlaceholderService,
  FetchUsersResponse,
  UpdateUserRequest,
} from 'types';

import api from 'helpers/api';

const placeholderService: PlaceholderService = {
  fetchUsers: () => api.get<FetchUsersResponse>('/users', {}),
  updateUser: (userId, fields) => api.patch<void, UpdateUserRequest>(`users/${userId}`, fields),
  deleteUser: (userId) => api.delete(`users/${userId}`, {}),
};

export default placeholderService;
