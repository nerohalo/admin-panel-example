import { ActionCreator } from 'redux';

import {
  ThunkResult,
  UserSetAction,
  UserUpdateAction,
  UserDeleteAction,
  User,
} from 'types';

import C from 'helpers/constants';

export const userSet: ActionCreator<UserSetAction> = (users: User[]) => ({
  users,
  type: C.ACTION_TYPES.USER_SET,
});

export const userUpdate: ActionCreator<UserUpdateAction> = (userId: number, fields: Partial<User>) => ({
  userId,
  fields,
  type: C.ACTION_TYPES.USER_UPDATE,
});

export const userDelete: ActionCreator<UserDeleteAction> = (userId: number) => ({
  userId,
  type: C.ACTION_TYPES.USER_DELETE,
});

export const fetchUsers = (): ThunkResult<Promise<User[]>> => (
  dispatch, getState, services,
): Promise<User[]> => services.placeholderService.fetchUsers()
  .then((users) => {
    dispatch(userSet(users));
    return users;
  });

export const updateUser = (userId: number, fields: Partial<User>): ThunkResult<Promise<void>> => (
  dispatch, getState, services,
): Promise<void> => services.placeholderService.updateUser(userId, fields)
  .then(() => {
    dispatch(userUpdate(userId, fields));
  });

export const deleteUser = (userId: number): ThunkResult<Promise<void>> => (
  dispatch, getState, services,
): Promise<void> => services.placeholderService.deleteUser(userId)
  .then(() => {
    dispatch(userDelete(userId));
  });
