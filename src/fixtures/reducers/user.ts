import { Fixture, UserReduxProps } from 'types';

// eslint-disable-next-line import/prefer-default-export
export const userReducerStateFixture: Fixture<UserReduxProps> = (state = {}) => ({
  data: [],
  loading: true,
  ...state,
});
