import { Fixture, ApplicationState } from 'types';

import { userReducerStateFixture } from './user';

export const rootReducerStateFixture: Fixture<ApplicationState> = (state = {}) => ({
  user: userReducerStateFixture(),
  ...state,
});

export * from './user';
