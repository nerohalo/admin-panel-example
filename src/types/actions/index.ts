import { ThunkAction, ThunkDispatch, ThunkMiddleware } from 'redux-thunk';

import {
  ApplicationState,
  UserActionTypes,
  ServiceProvider,
} from 'types';

export type ActionTypes = UserActionTypes;

export type ThunkResult<R> = ThunkAction<R, ApplicationState, ServiceProvider, ActionTypes>;
export type DispatchExts = ThunkDispatch<ApplicationState, ServiceProvider, ActionTypes>;
export type ThunkMiddlewareType = ThunkMiddleware<ApplicationState, ActionTypes, ServiceProvider>

export * from './user';
