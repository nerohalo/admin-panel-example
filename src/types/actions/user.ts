import { Action } from 'redux';

import { User } from 'types';

import C from 'helpers/constants';

export interface UserSetAction extends Action {
  type: typeof C.ACTION_TYPES.USER_SET;
  users: User[];
}

export interface UserUpdateAction extends Action {
  type: typeof C.ACTION_TYPES.USER_UPDATE;
  userId: number;
  fields: Partial<User>;
}

export interface UserDeleteAction extends Action {
  type: typeof C.ACTION_TYPES.USER_DELETE;
  userId: number;
}

export type UserActionTypes = UserSetAction
  | UserUpdateAction
  | UserDeleteAction;
