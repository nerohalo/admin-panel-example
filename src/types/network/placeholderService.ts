import { User } from 'types';

/**
 * GET /users
 */

export type FetchUsersResponse = User[]

/**
 * PATCH /users
 */

export type UpdateUserRequest = Partial<User>;
