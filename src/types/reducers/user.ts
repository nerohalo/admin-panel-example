import { User } from 'types';

export interface UserReduxProps {
  data: User[];
  loading: boolean;
}
