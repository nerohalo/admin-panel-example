import { UserReduxProps } from './user';

export * from './user';

export interface ApplicationState {
  user: UserReduxProps;
}
