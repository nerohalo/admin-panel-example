import {
  User,
} from 'types';

export interface PlaceholderService {
  fetchUsers: () => Promise<User[]>;
  updateUser: (userId: number, fields: Partial<User>) => Promise<void>;
  deleteUser: (userId: number) => Promise<void>;
}
export interface ServiceProvider {
  placeholderService: PlaceholderService;
}
