import * as userActions from 'actions/user';
import { userFixture, rootReducerStateFixture, userReducerStateFixture } from 'fixtures';
import { mockStore, mockServiceProvider, mockPlaceholderService } from 'mocks';

describe('user actions', () => {
  describe('fetchUsers thunk', () => {
    test('dispatches userSet action', () => {
      const users = [userFixture()];

      const serviceProvider = mockServiceProvider({
        placeholderService: mockPlaceholderService({
          fetchUsers: async () => users,
        }),
      });

      const store = mockStore(rootReducerStateFixture(), serviceProvider);

      return store.dispatch(userActions.fetchUsers()).then(() => {
        const actions = store.getActions();

        expect(actions).toContainEqual(userActions.userSet(users));
      });
    });
  });

  describe('updateUser thunk', () => {
    test('dispatches userUpdate action', () => {
      const user = userFixture();
      const user2 = userFixture({
        id: 2,
        name: 'Leanne Woods',
      });

      const serviceProvider = mockServiceProvider();

      const store = mockStore(rootReducerStateFixture({
        user: userReducerStateFixture({
          data: [user, user2],
        }),
      }), serviceProvider);

      return store.dispatch(userActions.updateUser(user2.id, { name: user2.name })).then(() => {
        const actions = store.getActions();

        expect(actions).toContainEqual(userActions.userUpdate(user2.id, { name: user2.name }));
      });
    });
  });

  describe('deleteUser thunk', () => {
    test('dispatches userDelete action', () => {
      const user = userFixture();
      const user2 = userFixture({
        id: 2,
        name: 'Leanne Woods',
      });

      const serviceProvider = mockServiceProvider();

      const store = mockStore(rootReducerStateFixture({
        user: userReducerStateFixture({
          data: [user, user2],
        }),
      }), serviceProvider);

      return store.dispatch(userActions.deleteUser(user2.id)).then(() => {
        const actions = store.getActions();

        expect(actions).toContainEqual(userActions.userDelete(user2.id));
      });
    });
  });
});
