import C from 'helpers/constants';

import { userReducerStateFixture, userFixture } from 'fixtures';
import reducer from 'reducers/user';

describe('users reducer', () => {
  it('handles USER_SET action', () => {
    const users = [userFixture()];

    const initialState = userReducerStateFixture();

    const action = {
      type: C.ACTION_TYPES.USER_SET,
      users,
    };

    const expectedState = userReducerStateFixture({
      data: users,
      loading: false,
    });

    expect(reducer(initialState, action)).toEqual(expectedState);
  });

  it('handles USER_UPDATE action', () => {
    const user = userFixture();
    const user2 = userFixture({
      id: 2,
    });

    const user2Modified = userFixture({
      id: 2,
      name: 'Leanne Woods',
    });

    const initialState = userReducerStateFixture({
      data: [user, user2],
      loading: false,
    });

    const action = {
      type: C.ACTION_TYPES.USER_UPDATE,
      userId: user2.id,
      fields: { name: user2Modified.name },
    };

    const expectedState = userReducerStateFixture({
      data: [user, user2Modified],
      loading: false,
    });

    expect(reducer(initialState, action)).toEqual(expectedState);
  });

  it('handles USER_DELETE action', () => {
    const user = userFixture();
    const user2 = userFixture({
      id: 2,
    });

    const initialState = userReducerStateFixture({
      data: [user, user2],
      loading: false,
    });

    const action = {
      type: C.ACTION_TYPES.USER_DELETE,
      userId: user2.id,
    };

    const expectedState = userReducerStateFixture({
      data: [user],
      loading: false,
    });

    expect(reducer(initialState, action)).toEqual(expectedState);
  });
});
