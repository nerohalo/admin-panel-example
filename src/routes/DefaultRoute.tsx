import React from 'react';
import { RouteProps, Route } from 'react-router-dom';

import DefaultLayout from 'layouts/DefaultLayout';

const DefaultRoute: React.FC<RouteProps> = ({ ...props }) => (
  <DefaultLayout>
    <React.Suspense fallback={<div>Loading...</div>}>
      <Route {...props} />
    </React.Suspense>
  </DefaultLayout>
);

export default DefaultRoute;
