import React, { lazy } from 'react';
import {
  BrowserRouter,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';

import DefaultRoute from './DefaultRoute';

const PageUsers = lazy(() => import('components/Pages/Users'));

const Router: React.FC = () => (
    <BrowserRouter>
        <Switch>
            <DefaultRoute path="/users" component={PageUsers}/>
            <Route render={(): React.ReactNode => <Redirect to="/users/list" />} />
            {/* Should have a not found page, but for demonstrative purposes redirect to users/list */}
        </Switch>
    </BrowserRouter>
);

export default Router;
