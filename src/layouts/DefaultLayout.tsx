import React from 'react';

import Header from 'components/Header';

import styles from './DefaultLayout.module.scss';

const DefaultLayout: React.FC = ({ children }) => (
  <>
    <Header />

    <div className={styles.container}>
      {children}
    </div>
  </>
);

export default DefaultLayout;
