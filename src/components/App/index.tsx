import React from 'react';

import Router from 'routes';

import styles from './App.module.scss';

const App: React.FC = () => (
  <div className={styles.app}>
    <Router />
  </div>
);

export default App;
