import classNames from 'classnames';
import React, { useEffect, useState, Fragment } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { ApplicationState, DispatchExts, User } from 'types';

import C from 'helpers/constants';
import { getCurrentPageItems } from 'helpers/pagination';
import { useSortableData } from 'helpers/sort';

import * as userActions from 'actions/user';
import Paginator from 'components/Paginator';
import DataTable from 'components/UI/DataTable';

import styles from './UsersDisplay.module.scss';

const UsersDisplay: React.FC = () => {
  const dispatch = useDispatch<DispatchExts>();
  const history = useHistory();
  const { data: users, loading } = useSelector((state: ApplicationState) => state.user);
  const [activePage, setActivePage] = useState(0);
  const [expandedColumn, setExpandedColumn] = useState<number | null>(null);
  const [sortKey, setSortKey] = useState<keyof User | null>(null);
  const [sortOrder, setSortOrder] = useState<string | null>(null);
  const items = useSortableData<User>(users, sortKey, sortOrder);

  const getSortIcon = (key: keyof User): React.ReactElement => {
    if (key !== sortKey) {
      return <div className={styles.sortIcon}>↕</div>;
    }

    return sortOrder === C.SORT_ORDER.ASC
      ? <div className={styles.sortIcon}>↓</div>
      : <div className={styles.sortIcon}>↑</div>;
  };

  const handleSort = (key: keyof User): void => { // TODO: could be improved, maybe a hook.
    if (key !== sortKey) {
      setSortOrder(C.SORT_ORDER.ASC);
      setSortKey(key);
      return;
    }

    switch (sortOrder) {
      case C.SORT_ORDER.ASC:
        setSortOrder(C.SORT_ORDER.DESC);
        setSortKey(key);
        break;
      case C.SORT_ORDER.DESC:
        setSortOrder(null);
        setSortKey(null);
        break;
      default:
        setSortOrder(C.SORT_ORDER.ASC);
        setSortKey(key);
        break;
    }
  };

  useEffect(() => {
    if (loading) {
      dispatch(userActions.fetchUsers()).then((fetchedUsers) => {
        setExpandedColumn(fetchedUsers[0].id);
      });
    }
  }, [loading, dispatch]);

  return (
    <div className={styles.container}>
      <DataTable>
        <DataTable.Head>
          <DataTable.Row>
            <DataTable.HeadCell onClick={(): void => handleSort('id')}>
              <div className="d-flex">
                <span className={styles.ellipsis}>User ID</span>

                {getSortIcon('id')}
              </div>
            </DataTable.HeadCell>

            <DataTable.HeadCell onClick={(): void => handleSort('name')}>
              <div className="d-flex">
                <span className={styles.ellipsis}>Name</span>

                {getSortIcon('name')}
              </div>
            </DataTable.HeadCell>

             <DataTable.HeadCell onClick={(): void => handleSort('email')}>
              <div className="d-flex">
                <span className={styles.ellipsis}>Email</span>
                {getSortIcon('email')}
              </div>
             </DataTable.HeadCell>

             <DataTable.HeadCell onClick={(): void => handleSort('phone')}>
              <div className="d-flex">
                <span className={styles.ellipsis}>Phone</span>
                {getSortIcon('phone')}
              </div>
             </DataTable.HeadCell>
          </DataTable.Row>
        </DataTable.Head>

        <DataTable.Body>
          {getCurrentPageItems(items, activePage, 5).map((user) => {
            const expanded = user.id === expandedColumn;
            return (
              <Fragment key={`user-${user.id}`}>
                <DataTable.Row
                  onClick={(): void => (setExpandedColumn(expanded ? null : user.id))}
                  className={classNames({ [styles.activeRow]: expanded })}
                >
                  <DataTable.Cell>{user.id}</DataTable.Cell>
                  <DataTable.Cell>{user.name}</DataTable.Cell>
                   <DataTable.Cell>{user.email}</DataTable.Cell>
                   <DataTable.Cell>{user.phone}</DataTable.Cell>
                </DataTable.Row>

                  {expanded && (
                    <DataTable.Row
                      key={`userDescription-${user.id}`}
                      className={styles.expandedContent}
                    >
                      <DataTable.Cell colSpan={4}>
                        <div className={styles.content}>
                          <button className="mr-3" onClick={() => history.push(`${user.id}/configure`)}>
                            Update user information
                          </button>

                          <button onClick={() => dispatch(userActions.deleteUser(user.id))}>
                            Delete user
                          </button>
                        </div>
                      </DataTable.Cell>
                    </DataTable.Row>
                  )}
              </Fragment>
            );
          })}
        </DataTable.Body>
      </DataTable>

      <Paginator
          activePage={activePage}
          itemsPerPage={5}
          totalItems={users.length}
          pageNeighbours={4}
          onChange={(value): void => setActivePage(value)}
      />
    </div>
  );
};

export default UsersDisplay;
