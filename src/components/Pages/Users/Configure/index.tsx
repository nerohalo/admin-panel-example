import React from 'react';
import { useSelector } from 'react-redux';
import {
  Redirect, Route, useParams,
} from 'react-router-dom';

import { ApplicationState } from 'types';

import Form from './Form';

import styles from './Configure.module.scss';

const Configure: React.FC = () => {
  const { userId } = useParams<{ userId: string }>();
  const { data: users, loading } = useSelector((state: ApplicationState) => state.user);

  const user = users.find((u) => u.id === Number(userId));

  if (loading) {
    return (
      <Route render={(): React.ReactNode => <Redirect to="/users/list" />} />
    );
  }

  if (!user) {
    return (
      <div>
        USER was not found!
      </div>
    );
  }

  return (
    <div className={styles.container}>
      <h1 className="d-flex">Modify user information</h1>

      <Form user={user} />
    </div>
  );
};

export default Configure;
