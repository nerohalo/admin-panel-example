import { yupResolver } from '@hookform/resolvers/yup';
import React from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import * as yup from 'yup';

import { DispatchExts, User } from 'types';

import * as userActions from 'actions/user';

import styles from './Form.module.scss';

interface FormProps {
  user: User;
}

interface FormData {
  name: string;
  email: string;
  phone: string;
}

const Form: React.FC<FormProps> = ({ user }) => {
  const dispatch = useDispatch<DispatchExts>();
  const history = useHistory();

  const {
    register, handleSubmit, formState: { errors, isDirty },
  } = useForm<FormData>({
    resolver: yupResolver(yup.object().shape({
      name: yup.string().required(),
      email: yup.string().email().required(),
      phone: yup.string().required(), // TODO: add libphonenumber validation, move yup schemas to helpers/validationSchemas.
    })),
    defaultValues: {
      name: user.name || '',
      email: user.email || '',
      phone: user.phone || '',
    },
  });

  const updateUser = (data: FormData): void => {
    if (user) {
      dispatch(userActions.updateUser(user.id, data)).then(() => {
        history.push('/list');
      });
    }
  };

  return (
    <form onSubmit={handleSubmit(updateUser)}>
      <label className={styles.label}>
        <div className={styles.labelText}>
          Name
        </div>

        <input
          className={styles.input}
          {...register('name')}
          aria-invalid={!!errors.name}
        />
        <div className={styles.inputError}>{errors.name?.message}</div>
      </label>

      <label className={styles.label}>
        <div className={styles.labelText}>
          Email
        </div>

        <input
          className={styles.input}
          {...register('email')}
          aria-invalid={!!errors.email}
        />
        <div className={styles.inputError}>{errors.email?.message}</div>
      </label>

      <label className={styles.label}>
        <div className={styles.labelText}>
          Phone
        </div>

        <input
          className={styles.input}
          {...register('phone')}
          aria-invalid={!!errors.phone}
        />
        <div className={styles.inputError}>{errors.phone?.message}</div>
      </label>

      <div className="d-flex my-4">
        <button type="submit" disabled={!isDirty}>
          update
        </button>
      </div>
    </form>
  );
};

export default Form;
