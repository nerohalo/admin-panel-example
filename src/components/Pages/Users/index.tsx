import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

import Configure from './Configure';
import UsersDisplay from './UsersDisplay';

const Users: React.FC = () => {
  const match = useRouteMatch();

  return (
    <Switch>
      <Route
        path={`${match.path}/list`}
        component={UsersDisplay}
      />

      <Route
        path={`${match.path}/:userId/configure`}
        component={Configure}
      />
    </Switch>
  );
};

export default Users;
