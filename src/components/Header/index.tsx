import React from 'react';
import { NavLink } from 'react-router-dom';

import styles from './Header.module.scss';

const Header: React.FC = () => (
    <header className={styles.header}>
      <nav className={styles.nav}>
        <ul className={styles.navList}>
            <li className={styles.navItem}>
                <NavLink
                    activeClassName={styles.navLinkActive}
                    className={styles.navLink}
                    to="/users/list"
                >
                    Users
                </NavLink>
            </li>
        </ul>
      </nav>
    </header>
);

export default Header;
