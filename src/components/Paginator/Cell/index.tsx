import classNames from 'classnames';
import React from 'react';

import styles from './Cell.module.scss';

interface CellProps extends React.HTMLAttributes<HTMLDivElement>{
  disabled?: boolean;
  active?: boolean;
  control?: boolean;
  onClick: () => void;
  label: string;
}

const Cell: React.FC<CellProps> = ({
  children,
  active = false,
  disabled = false,
  control = false,
  onClick,
  label,
}) => (
  <button
    type="button"
    className={classNames(styles.cell, { [styles.selected]: active, [styles.control]: control })}
    disabled={disabled || active}
    onClick={onClick}
    aria-label={label}
  >
    {children}
  </button>
);

export default Cell;
