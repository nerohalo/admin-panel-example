import React, { useMemo } from 'react';

import { generatePagesRange } from 'helpers/pagination';

import Cell from './Cell';

import styles from './Paginator.module.scss';

interface PaginatorProps {
  activePage: number;
  itemsPerPage: number;
  totalItems: number;
  pageNeighbours: number;
  onChange: (pageNumber: number) => void;
}

const Paginator: React.FC<PaginatorProps> = ({
  activePage,
  itemsPerPage,
  totalItems,
  pageNeighbours,
  onChange,
}) => {
  const totalPages = Math.max(1, Math.ceil(totalItems / itemsPerPage));

  const pagesRange = useMemo(
    () => generatePagesRange(totalPages, activePage, pageNeighbours),
    [totalPages, activePage, pageNeighbours],
  );

  return totalPages > 1 ? (
    <div className={styles.container}>
      <div className={styles.wrapper}>
        <Cell
          disabled={activePage === 0}
          onClick={(): void => onChange(activePage - 1)}
          control
          label="Paginate to previous page"
        >
          {'<'}
        </Cell>

        {pagesRange.map((pageIndex) => (
          <Cell
            key={`paginatorPage-${pageIndex}`}
            active={activePage === pageIndex}
            onClick={(): void => onChange(pageIndex)}
            label={activePage === pageIndex ? 'Current page selected' : `${pageIndex} page selected`}
          >
            {pageIndex + 1}
          </Cell>
        ))}

        <Cell
          disabled={activePage === totalPages - 1}
          onClick={(): void => onChange(activePage + 1)}
          control
          label="Paginate to next page"
        >
          {'>'}
        </Cell>
      </div>
    </div>
  ) : null;
};

export default Paginator;
