/* eslint-disable react/jsx-props-no-spreading */
import classNames from 'classnames';
import React from 'react';

import styles from './DataTable.module.scss';

const DataTableHead: React.FC<React.TableHTMLAttributes<HTMLTableSectionElement>> = ({
  children,
  className,
  ...props
}) => (
  <thead className={classNames(styles.head, className)} {...props}>{children}</thead>
);

const DataTableHeadCell: React.FC<React.ThHTMLAttributes<HTMLTableHeaderCellElement>> = ({
  children,
  className,
  ...props
}) => (
  <th className={classNames(styles.cell, className)} {...props}>{children}</th>
);

const DataTableBody: React.FC<React.TableHTMLAttributes<HTMLTableSectionElement>> = ({
  children,
  className,
  ...props
}) => (
  <tbody className={classNames(styles.body, className)} {...props}>{children}</tbody>
);

const DataTableRow: React.FC<React.TableHTMLAttributes<HTMLTableRowElement>> = ({
  children,
  className,
  ...props
}) => (
      <tr className={classNames(styles.row, className)} {...props}>{children}</tr>
);

const DataTableCell: React.FC<React.TdHTMLAttributes<HTMLTableCellElement>> = ({
  children,
  className,
  ...props
}) => (
  <td className={classNames(styles.cell, className)} {...props}>{children}</td>
);

interface DataTableComponent extends React.FC {
  Head: typeof DataTableHead;
  HeadCell: typeof DataTableHeadCell;
  Body: typeof DataTableBody;
  Row: typeof DataTableRow;
  Cell: typeof DataTableCell;
}

const DataTable: DataTableComponent = ({ children }) => (
  <table className={styles.table}>
    {children}
  </table>
);

DataTable.Head = DataTableHead;
DataTable.HeadCell = DataTableHeadCell;
DataTable.Body = DataTableBody;
DataTable.Row = DataTableRow;
DataTable.Cell = DataTableCell;

export default DataTable;
