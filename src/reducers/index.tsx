import { combineReducers } from 'redux';

import { ApplicationState } from 'types';

import user from './user';

const mainReducer = combineReducers<ApplicationState>({
  user,
});

export default mainReducer;
