import { Reducer } from 'redux';

import { UserActionTypes, UserReduxProps } from 'types';

import C from 'helpers/constants';

export const defaultState: UserReduxProps = {
  data: [],
  loading: true,
};

const reducer: Reducer<UserReduxProps, UserActionTypes> = (state = defaultState, action) => {
  switch (action.type) {
    case C.ACTION_TYPES.USER_SET:
      return {
        ...state,
        data: action.users,
        loading: false,
      };

    case C.ACTION_TYPES.USER_UPDATE:
      return {
        ...state,
        data: state.data.map((user) => {
          if (user.id === action.userId) {
            return ({
              ...user,
              ...action.fields,
            });
          }

          return user;
        }),
        loading: false,
      };

    case C.ACTION_TYPES.USER_DELETE:
      return {
        ...state,
        data: state.data.filter((user) => user.id !== action.userId),
        loading: false,
      };

    default:
  }

  return state;
};

export default reducer;
