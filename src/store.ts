/* eslint-disable no-underscore-dangle,@typescript-eslint/no-explicit-any */
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

import { ThunkMiddlewareType } from 'types';

import serviceProvider from 'services';

import mainReducer from './reducers';

const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ as typeof compose || compose;

const store = createStore(
  mainReducer,
  composeEnhancers(
    applyMiddleware(thunk.withExtraArgument(serviceProvider) as ThunkMiddlewareType),
  ),
);

export default store;
